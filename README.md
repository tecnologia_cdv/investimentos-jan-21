**Estágio Desenvolvedor**

Parabéns, você passou para a fase do teste técnico do processo seletivo do Clube do Valor.

---

## Instruções

Esse teste consiste em 2 etapas:

* Um teste de programação
* Algumas perguntas técnicas

Para evitar problemas no recebimento de e-mails é importante que as respostas para cada uma das etapas seja enviada seguindo os passos abaixo:

1. Criar um fork deste repositório, transformar ele em privado.
2. Adicionar o nosso usuário (tecnologia@clubedovalor.com.br) como colaborador.
3. Implementar os exercícios abaixo.
4. **Fazer upload do arquivo** contendo as respostas para as perguntas técnicas no **repositório**.
4. Enviar um e-mail para luana@clubedovalor.com.br com o assunto "[Teste Desenvolvedor] - Nome do candidato" e o link do repositório no corpo da mensagem.

Então vamos lá!


## Teste de Programação

### Escolha da plataforma/linguagem

Você pode criar a sua solução como um aplicativo command line ou um webapp em uma das plataformas abaixo.

* PHP > 7.0
* Python > 3

Pense no tipo de resultado que você quer alcançar e *escolha* o tipo de linguagem e aplicação *apropriados*.

### Requisitos da tarefa

* Resolva a tarefa abaixo
* Inclua instruções para compilação e execução da sua solução
* Fique à vontade para utilizar os frameworks / bibliotecas / pacotes **se** achar relevante.
** Atenção para incluir nas suas instruções instalações de eventuais dependencias. **Simplicidade é chave**.
* Evite incluir artefatos desnecessários no seu repositório (pacotes, arquivos compilados, ..)

### Tarefa

Para essa tarefa será utilizada a api de episódios do seriado The Office  (https://officeapi.dev/).

Como um **usuário**

Eu quero **poder informar duas datas**

Para **obter o número de episódios da serie The Office que foram lançados entre as duas datas informadas**

## Questões Técnicas

Por favor, responda as questões abaixo em um arquivo chamado `Respostas tecnicas.md`

1. Quanto tempo você utilizou para o teste de programação? O que você adicionaria a sua solução se tivesse mais tempo para fazê-lo? Se você não gastou muito tempo para desenvolver a solução essa é sua oportunidade para falar o que você faria a mais.
2. Como você encontraria um problema de performance em um sistema que está em produção? Você já teve que fazer isso? Como você faria para a solução que você criou?
3. Como você implementaria um relatório do sistema que você criou contendo todas as chamadas para ele? Considere que esse relatório deve ser enviado para um e-mail nas segundas-feiras às 10h.

## Critérios de Avaliação

* Estrutura/Organização do projeto
* Os resultados devem ser os esperados
* Ausência de bugs
* Limpeza do código
* Respostas para as questões técnicas

### Diferenciais (não obrigatórios)

* Testes


#### Obrigado por participar do teste!
- Equipe [Clube do Valor](https://clubedovalor.com.br/carreiras/)

`v1.0`